import 'react-native-gesture-handler';
import React from 'react';
import Navigator from './component/Navigator.js';
import { Provider } from 'react-redux';
import  store  from './redux/store';
// import Test from './src/screens/aatest';
export default App = () => {
  return (
    // Redux: Global Store
    <Provider store={store}>

        <Navigator/>


    </Provider>
  );
};
