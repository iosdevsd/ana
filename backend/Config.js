const BASE_URL = 'http://www.files.aqts.com.qa/api/';
const XApiKey = 'c3a3cf7c211b7c07b2495d8aef9761fc';

export const GOOGLE_MAPS_APIKEY = 'AIzaSyBlcqoYnME3Aa8BohjF6OlUzBFQ35SqTRs'
export const PATH_URL = {
  SignIn: 'Login/VarifyUser',
  SignUp: 'user/Create',
  RegisterOtp: '/register_otp',
  LoginOtp: 'Login/VarifyUser',
  GetProfile: '/getprofile',
  EditProfile: '/edit_user_profile',
  Project:'ProjectPerformance/GetProjectPerformance?UserId',
  Performance2:'ProjectPerformance/GetProjectSpeedoMtr?projectId',
  Cont:'ContractorPerformance/GetContractorPerformance?UserId',
  Contract :'ContractorPerformance/GetContractorSpeedoMtr?contractorId',
  Depart:'DepartmentPerformance/GetDepartmentPerformance?UserId',
  Dc:'DepartmentPerformance/GetDepartmentSpeedoMtr?departmentId',
  Tc:'DepartmentPerformance/GetDepartmentTrendingChart?departmentId',
  Tcs:'ContractorPerformance/GetContractorTrendingChart?contractorId',
  Pcs:'ProjectPerformance/GetProjectTrendingChart?projectId',
  Search:'ProjectPerformance/GetProjectsNameByCodeAndUserId?projStartName',
  Code:'ProjectPerformance/GetContractorsByContNameAndUserId?contractorName',
  for:'ForgetPassword/GetPassword',
  ch:'ChangePassword/UpdatePassword',
};



export const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    'X-API-KEY': XApiKey,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};
export const ApiSauceMultipart = {
  baseURL: BASE_URL,
  headers: {
    'X-API-KEY': XApiKey,
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
};
export const latitudeDelta = 0.0922;
