import AsyncStorage from '@react-native-community/async-storage';
import {PATH_URL} from './Config';
import ApiSauce from './ApiSauce';
import ApiSauceMu from './ApiSauce';

import {Dimensions} from 'react-native';
import {latitudeDelta} from '../backend/Config';
const request = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then((response) => {
    // alert(JSON.stringify(response))
      if (response.ok) {
      //  console.log(response)
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};


const getrequest = (path) => {
  return new Promise((resolve, reject) => {
    ApiSauce.get(path).then((response) => {
    //  alert(JSON.stringify(response))
      if (response.ok) {
      //  console.log(response)
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};

const requestMultipart = (path, formdata) => {
  //alert(path)
  console.log(formdata)
  return new Promise((resolve, reject) => {
    ApiSauceMu.post(path, formdata).then((response) => {
      if (response.ok) {
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};

export const SignUps = (form) => requestMultipart(PATH_URL.SignUp, form);
export const SignInApi = (json) => request(PATH_URL.SignIn, json);
export const Forg = (json) => request(PATH_URL.for, json);
export const Changs = (json) => request(PATH_URL.ch, json);
export const RegisterOtpApi = (json) => request(PATH_URL.RegisterOtp, json);
export const LoginOtpApi = (json) => request(PATH_URL.SignIn, json);
export const GetProfileApi = (json) => request(PATH_URL.GetProfile, json);
export const Perofrmance = (path) => getrequest(path);
export const Perofrmance2 = (path) => getrequest(path);
export const Conrtact = (path) => getrequest(path);
export const Dep = (path) => getrequest(path);
export const Charts = (path) => getrequest(path);
export const DCharts = (path) => getrequest(path);
export const Trending = (path) => getrequest(path);
export const Trendings = (path) => getrequest(path);
export const AsyncStorageSetUser = (user) =>
  AsyncStorage.setItem('user', JSON.stringify(user));
export const AsyncStorageGetUser = () => AsyncStorage.getItem('user');
export const AsyncStorageClear = () => AsyncStorage.clear();





export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;


export const formatAmount = (amount) =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = (str) =>
  str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = (price) => `\u20B9 ${price}`;
