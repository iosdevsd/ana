import { create } from 'apisauce';
import { ApiSauceJson ,ApiSauceMultipart} from './Config';
const ApiSauce = create(ApiSauceJson);
export const ApiSauceMu = create(ApiSauceMultipart);

export default ApiSauce;
