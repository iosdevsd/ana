import {StyleSheet, Platform,Dimensions} from 'react-native';
const window = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'white'

    },
    imagebackground:{
      width:'100%',
      height:'100%'
    },
    imageprofile:{
      flexDirection:'row',
      alignSelf:'center',
      width:100,
      height:100,
      marginTop:50,
      borderRadius:50,
    },
    introdu:{
      flexDirection:'row',
      margin:10,
    },
    homeView:{
      flexDirection:'row',
      justifyContent:'space-between',
      marginTop:5
    },
    homehi:{

      height:20,
      marginLeft:10

    },
    homeImage:{
      width:30,
      height:30,
      marginLeft:12,
      resizeMode:'contain',
      marginRight:12
    },
    byclick:{color:'#ACB1C0',fontSize:12,height:100,fontFamily:'Nunito-Regular'},
    term:{color:'#6248B3',fontSize:12,textDecorationLine:'underline'},
    mobile:{width:23,height:22,resizeMode:'contain',marginLeft:6,marginTop:7},
      mobiles:{width:23,height:22,resizeMode:'contain',marginLeft:4,marginTop:1},
    mobiletext:{marginLeft:2,color:'white',fontSize:16,marginTop:9,fontWeight:'bold',fontFamily:'Nunito-Bold'},
    mobiletexts:{marginLeft:2,color:'white',fontSize:16,marginTop:1,fontWeight:'bold',fontFamily:'Nunito-Bold'},
    googletext:{marginLeft:4,color:'black',fontSize:16,marginTop:9,fontWeight:'bold',fontFamily:'Nunito-Bold'},
    continue:{color:'black',fontSize:15,alignSelf:'center',marginTop:10,fontFamily:'Nunito-Regular'},
    facebookview:{width:120,margin:0,backgroundColor:'#3B5998',borderRadius:13,height:40,flexDirection:'row'},
        googleview:{width:120,marginLeft:5,backgroundColor:'#FFFFFF',borderRadius:13,height:40,flexDirection:'row',shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 2,
},
shadowOpacity: 0.25,
shadowRadius: 3.84,

elevation: 5},
    mainlogo:{
      height:60,
      width:160,
      alignSelf:'center',
      resizeMode:'contain',
      marginTop:80,
    },
    loginlogo:{
      height:40,
      width:90,

      resizeMode:'contain',
      marginTop:60,
    },
    introimage:{
      width:130,
      height:73.58,
      resizeMode:'contain',
      marginLeft:30,
      marginTop:86,
    },
    next:{
      color:'white',
      alignSelf:'center',
      fontSize:18,
      fontWeight:'bold',
      marginTop:8
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "black",
    },
    otpstyle:{width: '80%', height: 100,alignSelf:'center'},
    resend:{color:'#6248B3',fontFamily:'Nunito-Bold',marginLeft:window.width - 150,marginTop:1},

    underlineStyleBase: {
        width: 65,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        borderColor:'black',
        color:'black',

        fontWeight: 'bold',
        fontSize:22,
    },

    underlineStyleHighLighted: {
        borderColor: "#eaecef",
    },
    qbs:{width:window.width -12,margin:4,height:130,marginTop:-14},
    must:{fontSize:12,color:'white',fontFamily:'Nunito-Regular',marginLeft:10},
      qbsimage:{width:60,height:60,resizeMode:'contain',margin:8,marginTop:15,marginLeft:10},
      question:{color:'white',marginTop:20,marginLeft:5,fontSize:21,fontWeight:'bold',fontFamily:'Nunito-Bold'},
    introtext:
      {color:'#273253',fontSize:13,width:window.width - 70,margin:6,fontFamily:'Nunito-Bold'},
      neetext:
        {color:'#273253',fontSize:16,width:window.width - 70,margin:12,fontFamily:'Nunito-Bold',marginTop:1},
      bottomview:{position:'absolute',bottom:30,backgroundColor:'#6248B3',height:40,borderRadius:20,width:window.width - 60,alignSelf:'center'},
        loginview:{marginTop:30,backgroundColor:'#8E1B56',height:40,borderRadius:9,width:window.width - 60,alignSelf:'center'},
        dont :{marginTop:20,color:'#000521',fontSize:14,fontFamily:'Nunito-SemiBold',alignSelf:'center',margin:20,textAlign:'center',lineHeight:20},
signup:{marginLeft:3,marginTop:20,color:'#8E1B56',fontSize:14,fontFamily:'Nunito-SemiBold',alignSelf:'center'},
        mainbottomview:{position:'absolute',bottom:170,backgroundColor:'#8E1B56',height:40,borderRadius:20,width:window.width - 60,alignSelf:'center'},

    logo:
      {
          width: 182,
          height: 250,
          alignSelf:'center',
          resizeMode:'contain',
          marginTop :window.height/2 - 125,



    },
    introduction:{fontSize:30,fontWeight:'bold',marginTop:30,alignSelf:'center'},
    welcome:{fontSize:22,fontWeight:'bold',marginTop:10,alignSelf:'center',fontFamily:'Nunito-Bold'},
    loginwith:{fontSize:27,width:200,fontWeight:'bold',marginTop:10,fontFamily:'Nunito-Bold',marginLeft:30},
    introtexts:
      {color:'#273253',fontSize:13,width:window.width - 70,margin:6,fontFamily:'Nunito-Regular',marginLeft:30},
      introtextss:
        {color:'#ACB1C0',fontSize:13,width:window.width - 70,margin:6,fontFamily:'Nunito-Regular',marginLeft:30,marginTop:20},
        textInput: {
    alignSelf: 'stretch',
    padding: 10,
    marginLeft: 20,
    borderBottomColor:'#ACB1C0',
    margin:5,
    marginRight:40,

    borderBottomColor: '#ACB1C0', // Add this to specify bottom border color
    borderBottomWidth: 1     // Add this to specify bottom border thickness
}

});
