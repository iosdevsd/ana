import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,TextInput,Dimensions,FlatList} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');

import * as actions from '../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {LoginOtpApi,SignUpApi,AsyncStorageSetUser,SignInApi,HomeApi,District,Rabi} from '../backend/Api';
import store from '../redux/store'

import Carousel from 'react-native-banner-carousel';
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;

const images = [
    "http://139.59.76.223/slider/slider-1.png",
    "http://139.59.76.223/slider/slider-2.png",
    "http://139.59.76.223/slider/slider-3.png"
];
import stringsoflanguages from './Language';



const Home = ({navigation}) =>{
    const [banner, setBanner] = useState([]);
const [array, seta] = useState([]);

    const loginButtonPress = (index) =>{
    if (index == 0){
      navigation.navigate('ProjectPerformance')
    }
  else  if (index == 1){
      navigation.navigate('Contractor')
    }
    else  if (index == 2){
        navigation.navigate('Department')
      }
      else  if (index == 3){
          navigation.navigate('Other')
        }
      else  if (index == 5){
          navigation.navigate('Change')
        }
        else  if (index == 4){
            navigation.navigate('Help')
          }
    else if (index == 6){
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Login'}],
                 });
    }
    }

    const renderItem=({item,index}) => {

    return(
     <TouchableOpacity onPress={() => loginButtonPress(index)} >
      <View style = {{elevation:5,width:window.width/2- 20,height:176 ,margin:10,backgroundColor:'white',shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
        height: 0,
        width: 0
    },
    //android
    elevation: 1}}>



                    <Image
                                 style={{width:100,height:100,marginTop:10,resizeMode:'contain',alignSelf:'center'}}
                                 source={item.image}
                             />

                             <Text style = {{color:'black',alignSelf:'center',textAlign:'center',fontSize:16,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold"}}>
     {item.text}
                             </Text>



           </View>

    </TouchableOpacity>

     );
    }

useEffect (() =>{
  //alert(stringsoflanguages.getLanguage())
  var array = [
    {
      image:require('../resources/Group3.png'),
      text:stringsoflanguages.project
    },
    {
      image:require('../resources/Group4.png'),
          text:stringsoflanguages.contractor
    },
    {
      image:require('../resources/Group5.png'),
      text:stringsoflanguages.depart
    },
    {
      image:require('../resources/Group6.png'),
        text:stringsoflanguages.other
    },
    {
      image:require('../resources/Group7.png'),
        text:stringsoflanguages.help,
    },
    {
      image:require('../resources/Group7.png'),
      text:stringsoflanguages.change,
    },

  ];
seta(array)
//   HomeApi({user_id:store.getState().user.user_id,deviceID:"dd",deviceType:"android",deviceToken:"ss",model_name:"Ss",carrier_name:"Ss",device_country:"ss",device_memory:"Ss",has_notch:"ss",manufacture:"ss",ip_address:"sss"})
//        .then((data) => {
//          console.log(JSON.stringify(data))
//          if (data.status) {
//           // alert(JSON.stringify(data.banners))
//            console.log(JSON.stringify(data.banners))
// setBanner(data.banners)
//          //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//          } else {
//            alert(data.msg)
//           // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//          }
//        })
//        .catch((error) => {
//          console.log('error', error);
//        });
//
//
//
//
//
//        District({user_id:"1"})
//             .then((data) => {
//               console.log(JSON.stringify(data))
//               if (data.status) {
//               //  alert(JSON.stringify(data))
//                 console.log(JSON.stringify(data.banners))
//    actions.District(data.list);
//               //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//               } else {
//                 alert(data.msg)
//                // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//               }
//             })
//             .catch((error) => {
//               console.log('error', error);
//             });
//
//
//             Rabi({user_id:"1"})
//                  .then((data) => {
//                    console.log(JSON.stringify(data))
//                    if (data.status) {
//
//                      //console.log(JSON.stringify(data.banners))
//         actions.Rabi(data.list);
//                    //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//                    } else {
//                      alert(data.msg)
//                     // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//                    }
//                  })
//                  .catch((error) => {
//                    console.log('error', error);
//                  });


},[])

const logout = () =>{
  actions.Logout({});
         navigation.reset({
                         index: 0,
                         routes: [{name: 'Introduction'}],
                       });
}
const renderPage = (image, index) => {
      //    alert(JSON.stringify(image))
      return (

          <View key={index} style={{elevation:2,width:'100%'}}>
              <Image style={{ width:Dimensions.get('window').width, height: 220,resizeMode:'contain'}} source={{ uri: image }} />
          </View>


      );
  }

  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView style = {{color:'white'}}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/background_slide.png') }>

      <View style = {{flexDirection:'row',justifyContent:'space-between'}}>
      <Image
                   style={{width:100,height:70,marginTop:20,resizeMode:'contain'}}
                   source={require('../resources/login-logo.png')}
               />
                    <TouchableOpacity onPress={() => logout()} >
               <Image
                            style={{width:30,height:30,marginTop:20,marginRight:20,resizeMode:'contain'}}
                            source={require('../resources/logout.png')}
                        />
                        </TouchableOpacity>

              </View>



              <View style={{width:'100%',marginTop:25}}>
                                 <Carousel
                                         autoplay
                                         autoplayTimeout={5000}
                                         loop
                                         pageIndicatorStyle={{ height: 7, width: 7, borderRadius: 3.5, backgroundColor: '#FFFFFF80' }}
                                         pageIndicatorContainerStyle={{ position: 'absolute', bottom: 10 }}
                                         activePageIndicatorStyle={{ height: 7, width:14, borderRadius:3.5, backgroundColor: '#08C25E' }}
                                         index={0}
                                         pageSize={Dimensions.get('window').width}
                                     >
                                         {images.map((image, index) => renderPage(image, index))}
                                     </Carousel>


</View>

<FlatList style={{alignSelf:'center'}}
      data={array}
      numColumns={2}


      renderItem={renderItem}
      />

      </ImageBackground>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )

}
export default Home;
