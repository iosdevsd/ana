import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,FlatList} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
import store from '../redux/store';
import SearchBar from 'react-native-search-bar';
const BASE_URL = 'http://www.files.aqts.com.qa/api/';
import MapView, { Marker } from 'react-native-maps';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import stringsoflanguages from './Language';
import moment from  'moment';
import ProgressCircle from 'react-native-progress-circle'
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import GetLocation from 'react-native-get-location';

import {
  PATH_URL,
} from '../backend/Config';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  SignUps,
  Perofrmance,
  Perofrmance2,
  Conrtact,
  Dep,
} from '../backend/Api';

const Department = ({navigation,route}) => {
  const [name, setName] = useState('');
    const [email, setEmail] = useState('');
      const [company, setCompany] = useState('');
      const [latitude, setLatiude] = useState(0);
      const [longitude, setLongitude] = useState(0);
        const [description, setDescription] = useState('');
        const [marker,setMarker] = useState([
        ])

useEffect (() =>{
//  alert(JSON.stringify(route.params.item))

  var project = JSON.stringify(PATH_URL.Project)
  console.log("22"+project)

var c = `${BASE_URL}${PATH_URL.Depart}=${store.getState().user.Id}`;


Dep(c)
     .then((data) => {
      console.log(JSON.stringify(data))
setMarker(data)
//  alert(JSON.stringify(data))
       if (data.Status) {

         //setMarker
      //   alert(data.otp)


       } else {

       }
     })
     .catch((error) => {
       console.log('error', error);
     },[]);
// http://www.files.aqts.com.qa/api/"ProjectPerformance/GetProjectPerformance?UserId"=1

//alert(JSON.stringify(PATH_URL))
//   GetLocation.getCurrentPosition({
//       enableHighAccuracy: true,
//       timeout: 15000,
//   })
//   .then(location => {
//       console.log(location);
// setLatiude(location.latitude)
// setLongitude(location.longitude)
//       // const [latitude, setLatiude] = useState(0);
//       // const [longitude, setLongitude] = useState(0);
//   })
//   .catch(error => {
//       const { code, message } = error;
//       console.warn(code, message);
//   })
},[])

const hello = (marker) =>{
  navigation.navigate('DChart',{
    item:marker
  })
}


const searchFilterFunction = (text)=>{
//
  // if (marker.length == 0){
  //   return
  // }
  //alert(text)

  if (text == ""){
    return
  }


  setName(text)


  var c = `${BASE_URL}DepartmentPerformance/GetDepartmentsByDeptNameAndUserId?departmentName=${text}&userId=${store.getState().user.Id}`;
console.log(c)

  Dep(c)
       .then((data) => {
        // ssssalert(JSON.stringify(data))
setMarker(data)
    //  alert(JSON.stringify(data))
//  setMarker(data)
  //  alert(JSON.stringify(data))
         if (data.Status) {

           //setMarker
        //   alert(data.otp)


         } else {

         }
       })
       .catch((error) => {
         console.log('error', error);
      })



  //  this.setState({filteredData: filteredData})


//   setName(text)
//   // if (name.length <= 1){
//   //   setProduct([])
//   //   return
//   // }
//   GlobalSearch({search: text})
//      .then((data) => {
//
//        if (data.status) {
//     console.log(JSON.stringify(data))
// setProduct(data.products)
//     //   alert(JSON.stringify(data))
//        } else {
//          alert(data.message)
//
//        }
//      })
//      .catch((error) => {
//        console.log('error', error);
//      });
    }
const renderItemProducts = ({ item, index }) => {


  var color = "green"
  var bgcolor = "#fff"


  if (index % 5 == 0){
     color = "#34B53A"
     bgcolor = "#34B53A20"
  }
  else if (index % 5 == 1){
    color = "#4339F2"
    bgcolor = "#4339F220"
  }
  else if (index % 5 == 2){
    color = "#FF3A29"
    bgcolor = "#FF3A2920"
  }
  else if (index % 5 == 3){
    color = "#02A0FC"
    bgcolor = "#02A0FC20"
  }
  else if (index % 5 == 4){
    color = "#9C27B0"
    bgcolor = "#9C27B020"
  }

  // navigation.navigate('Performances',{
  //   item:marker
  // })
    // alert(JSON.stringify(item))
    return (

  <TouchableOpacity onPress={() => hello(item)} >
      <View style = {{backgroundColor:'white',margin:'5%',elevation:5,margin:10,flexDirection:'row'}}>
      <Image
                   style={{width:40,height:40,margin:10,resizeMode:'contain'}}
                   source={require('../resources/contract.png')}
               />
<Text style = {{color:'#000521',fontSize:16,fontFamily:'Nunito-Bold',marginTop:7,margin:12,textalign:'center',marginTop:15,width:window.width - 100}}>
{item.DepartmentName}

</Text>
</View>
</TouchableOpacity>

    )
}
  return (

<View style = {{backgroundColor:'white'}}>

<SearchBar style = {{height:50,margin:10}}
textColor = {'black'}
placeholder= ''
value={name}
   onChangeText={text => searchFilterFunction(text)}
/>

<FlatList style = {{marginTop:20}}
                        data={marker}

                        renderItem={renderItemProducts}
                    />
</View>

  );
};

export default Department;
