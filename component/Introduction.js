import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,TextInput,ActivityIndicator} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
import stringsoflanguages from './Language';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
} from '../backend/Api';


import * as actions from '../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const Introduction = ({navigation}) => {
  const [userid, setUserid] = useState('');
    const [password, setPassword] = useState('');
const [loading, setLoading] = useState(false);

const signupButtonPress = () =>{
  navigation.navigate('Signup')
}


const onChanged  = (text) =>{
  setUserid(text.replace(/[^.a-z0-9]/gi,''))
   // this.setState({
   //     mobile: text.replace(/[^0-9]/g, ''),
   // });
}
const loginButtonPress = async () =>{
  if (userid.length == 0){
    alert('Please enter Userid')
    return
  }
  if (password.length == 0){
    alert('Please enter password')
    return
  }
setLoading(true)
var k = {UserId: userid,Password:password}


    LoginOtpApi({UserId: userid,Password:password})
         .then((data) => {
setLoading(false)
           if (data == "Please enter valid userId and password." || "User does not exist"){

             if (typeof data  == "string"){
               alert(data)
             }
             // if (Object.keys(data).length === 0 && data.constructor === Object) {
             //  alert(data)
             // }
            // return
           }
           console.log(JSON.stringify(data))
           if (data.Status) {
          //   alert(data.otp)
          actions.Login(data);
          AsyncStorageSetUser(data);
          navigation.reset({
                          index: 0,
                          routes: [{name: 'Home'}],
                        });

           } else {

           }
         })
         .catch((error) => {
           setLoading(false)
           alert('Invalid Credentials')
           console.log('error', error);
         });

  // navigation.reset({
  //                index: 0,
  //                routes: [{name: 'Home'}],
  //              });
  // if (mobile.length == 10){
  //   LoginOtpApi({mobile: +mobile})
  //        .then((data) => {
  //          console.log(JSON.stringify(data))
  //          if (data.status) {
  //         //   alert(data.otp)
  //          navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
  //
  //          } else {
  //            navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
  //          }
  //        })
  //        .catch((error) => {
  //          console.log('error', error);
  //        });
  // }else{
  //   alert('Invalid Mobile number')
  // }



}
  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'} >

{loading == true && (
  <View style={{flex:1,backgroundColor:'white'}}>
                  <ActivityIndicator style = {{position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'}}

                                     size="large" color= 'orange' />
              </View>
)}

  <View>
      <Image
                   style={style.introimage}
                   source={require('../resources/login-logo.png')}
               />


               <Text style = {{color:'black',fontFamily:'Nunito-Bold',fontSize: 36,paddingRight:30,marginTop:20,marginLeft:20}}>
                                    {stringsoflanguages.glad}
                                  </Text>

                                  <View style = {{marginLeft:28,width:window.width -56,marginTop:29}}>
                                  <TextInput
                                                             style={{ height: 40, width: '100%', fontSize: 16, fontFamily: 'Nunito-Bold', color: '#1D1E2C',borderBottomWidth: 1,
  borderBottomColor: "#acb1c0" }}
                                                             placeholder= {stringsoflanguages.user}
                                                             placeholderTextColor="#1D1E2C"
                                                             onChangeText={(text) => onChanged(text)}
                                                             value={userid}

                                                         />


                                                         

                                                        <TextField
                                                            label={stringsoflanguages.password}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {password}
                                                            secureTextEntry = {true}
                                                            onChangeText={(text) => setPassword(text)}


                                                        />

                         <Text onPress={() => navigation.navigate('Forgot')} style = {{alignSelf:'flex-end',fontSize:18,fontFamily:'Nunito-SemiBold',marginTop:16,color:'#8E1B56'}}>
                          {stringsoflanguages.forgot}
                         </Text>

                         {loading == false && (
                           <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                  <View>
                                                                  <Text style = {style.next}>
                                                                  {stringsoflanguages.login}
                                                                  </Text>

                                                                  </View>
                                                                  </TouchableOpacity>
                         )}





                                                        </View>

</View>

      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Introduction;
