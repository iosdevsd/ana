import React ,{ useEffect } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View} from 'react-native';
import style from '../style/Style.js';
import { AsyncStorageGetUser,AsyncStorageGettoken } from '../backend/Api';
import * as actions from '../redux/actions';
import store from '../redux/store'
const Splash = ({navigation}) => {
  const screenHandler = async () => {
    console.log(await AsyncStorageGettoken())

     actions.Login(JSON.parse(await AsyncStorageGetUser()))
       actions.Token(await AsyncStorageGettoken())
    setTimeout(() => {
      const user = store.getState().user
  if (Object.keys(user).length === 0 && user.constructor === Object) {
    navigation.replace('Introduction')
  } else {

    navigation.replace('TabNavigator')
  }
}, 1000);

  }
  useEffect(() => {
    screenHandler()
  }, []);
  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/splash.png') }>
      

      </ImageBackground>
    </View>
  );
};

export default Splash;
