import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,TextInput} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import stringsoflanguages from './Language';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  SignUps,
} from '../backend/Api';
const Signup = ({navigation}) => {
  const [name, setName] = useState('');
  const [userid, setUserid] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setmobile] = useState('');
      const [company, setCompany] = useState('');
        const [description, setDescription] = useState('');


const loginButtonPress = () =>{
  //UserId

  if (userid.length == 0){
    alert('Please enter userid')
    return
  }
else if (name.length == 0){
  alert('Please enter Name')
  return
}
if (email.length == 0){
  alert('Please enter Email')
  return
}
if (company.length == 0){
  alert('Please enter Company')
  return
}
if (description.length == 0){
  alert('Please enter Description')
  return
}

    SignUps({Name: name,Email:email,Mobile:null,DeviceName:'android',CompanyName:company,Description:description,UserId:userid})
         .then((data) => {
          // alert(JSON.stringify(data))
           if (data.Status) {
           alert(data.Message)


           } else {

           }
         })
         .catch((error) => {
           console.log('error', error);
         });
}


// const  onChanged = (text) =>{
//   //alert(text)
//   setUserid(text.replace(/[^0-9]/g, ''))
// }

const onChanged  = (text) =>{
  setUserid(text.replace(/[^.a-z0-9]/gi,''))
   // this.setState({
   //     mobile: text.replace(/[^0-9]/g, ''),
   // });
}

  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}>


      <Image
                   style={style.introimage}
                   source={require('../resources/login-logo.png')}
               />


               <Text style = {{color:'black',fontFamily:'Nunito-Bold',fontSize: 36,paddingRight:30,marginTop:20,marginLeft:20}}>
                                    {stringsoflanguages.enter}
                                  </Text>

                                  <View style = {{marginLeft:28,width:window.width -56,marginTop:29}}>



                                                        <TextField
                                                            label={stringsoflanguages.name}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {name}
                                                            onChangeText={(text) => setName(text)}


                                                        />

                                                        <TextField
                                                            label={stringsoflanguages.email}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {email}

                                                            onChangeText={(text) => setEmail(text)}


                                                        />

                                                        <TextInput
                                                                                   style={{ height: 40, width: '100%', fontSize: 16, fontFamily: 'Nunito-Bold', color: '#1D1E2C',borderBottomWidth: 1,
                        borderBottomColor: "#acb1c0" }}
                                                                                   placeholder= {stringsoflanguages.user}
                                                                                   placeholderTextColor="#1D1E2C"
                                                                                   onChangeText={(text) => onChanged(text)}
                                                                                   value={userid}

                                                                               />
                                                        <TextField
                                                            label={stringsoflanguages.company}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {company}

                                                            onChangeText={(text) => setCompany(text)}


                                                        />
                                                        <TextField
                                                            label={stringsoflanguages.description}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {description}

                                                            onChangeText={(text) => setDescription(text)}


                                                        />



                         <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                <View>
                                                                <Text style = {style.next}>
                                                                {stringsoflanguages.sign}
                                                                </Text>

                                                                </View>
                                                                </TouchableOpacity>

                                                                <TouchableOpacity onPress={() => navigation.goBack()} >

                                                                 <Text style = {style.dont}>
{stringsoflanguages.already}<Text style = {style.signup}>
&nbsp;{stringsoflanguages.login}

</Text>
                                                                 </Text>
                                                                 </TouchableOpacity>

                                                        </View>



      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Signup;
