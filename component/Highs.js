import React from 'react';
import { StyleSheet, View } from 'react-native';
import HighchartsReactNative from '@highcharts/highcharts-react-native';
const BASE_URL = 'http://www.files.aqts.com.qa/api/';
import moment from 'moment';
import stringsoflanguages from './Language';
var dates = ["2019-11-07T00:00:00", "2019-11-07T00:00:00"];
var ischeck = false;
import {
  PATH_URL,
} from '../backend/Config';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  Trending
} from '../backend/Api';
var pubKeyHashCount = [
    {
        "x": "2018-Oct-31",
        "y": 1566565
    },
    {
        "x": "2018-Nov-30",
        "y": 20540527
    },
    {
        "x": "2018-Dec-31",
        "y": 1244447
    },
    {
        "x": "2019-Jan-31",
        "y": 1341308
    },
    {
        "x": "2019-Feb-28",
        "y": 451458
    },
    {
        "x": "2019-Mar-31",
        "y": 368153
    }
];
export default class Highs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chartOptions: {},
        };
    }

    componentDidMount () {

      ischeck = false

if (this.props.route.params.item.CategoryId == "1" ||this.props.route.params.item.CategoryId == "2" ||this.props.route.params.item.CategoryId == "3"){
  ischeck = true
}


    //  alert(JSON.stringify(this.props.route.params.item))
      var c = `${BASE_URL}${PATH_URL.Tc}=${this.props.route.params.item.Id}&categoryId=${this.props.route.params.item.CategoryId}`;


      Trending(c)
           .then((data) => {

             var date = []
             var se = []

             if (data.length != 0){
               for (var i = 0; i<data.length;i++){
                    var k = ""

                 if (ischeck == true){
                     k = moment(data[i].PerformanceDate).format("MMM-YYYY")
                 }else{
                     k = moment(data[i].PerformanceDate).format("DD-MMM-YYYY")
                 }

                 //alert(k)
                 date.push(k)
                 se.push(data[i].PercentageYes)
               }
             }

             console.log(JSON.stringify(date))
          //  alert(JSON.stringify(date))



            var k =  {
              chart: {
               pinchType: 'x',

               scrollbar:true
           },

                series: [{
                   showInLegend: false,
                    data: se,
                    tooltip: {
      pointFormat: ''
    },
                }],



                xAxis: {

                        categories: date,
                        type: 'datetime',

                        labels: {
                            rotation: -65,

                        },


                    },
                    title: {
       text: stringsoflanguages.trend,
       align: 'center',
       y: 20 //  this to move y-coordinate of title to desired location
   },





                    yAxis: {
              allowDecimals: true,
              min: 0,
              max: 100,
              title: {
                  text: ischeck == true ? stringsoflanguages.percentage:stringsoflanguages.audit
              }
          },
            }

  this.setState({chartOptions:k})

    //  setMarker(data)
      //  alert(JSON.stringify(data))
             if (data.Status) {

               //setMarker
            //   alert(data.otp)


             } else {

             }

           })
           .catch((error) => {
             console.log('error', error);
          })
        }


    render() {
        return (
            <View style={styles.container}>
                <HighchartsReactNative
                    styles={styles.container}
                    options={this.state.chartOptions}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        flex: 1
    }
});
