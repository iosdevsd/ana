import React ,{ useState, useEffect } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,TextInput} from 'react-native';
import style from '../style/Style.js';
import OTPInputView from '@twotalltotems/react-native-otp-input'

import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
} from '../backend/Api';


import * as actions from '../redux/actions';


const Otp = ({route,navigation}) => {
  const [state, setState] = useState({
    code: '',
    otp:route.params.otp,
    mobile:route.params.mobile,
    status:route.params.status,

  });

  const verifyMobile = () =>{

    if (state.code.length == 0){
      alert('Please enter Otp')
    }else if (state.code != state.otp){
      alert('Otp not match')
    }else{

      switch (state.status) {
        case true:
        SignInApi({mobile: state.mobile, otp: state.otp})
          .then((data) => {
       console.log(JSON.stringify(data))
       if (data.status) {
actions.Login(data.result);
AsyncStorageSetUser(data.result);
navigation.reset({
                index: 0,
                routes: [{name: 'Home'}],
              });
       } else {

       }
     })
     .catch((error) => {
       console.log('error', error);
     });
          break;
          case false:
          navigation.navigate('Signup',{mobile:state.mobile})
          break;
        default:

      }

    }
  }

  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/background_slide.png') }>
      <Image
                   style={style.loginlogo}
                   source={require('../resources/login-logo.png')}
               />
               <Text style = {style.loginwith}>
              Phone Verification
               </Text>
               <Text style = {style.introtexts}>
              Enter your OTP code here
               </Text>

               <Text style = {style.introtextss}>
               We have sent you an SMS with a code to number +91 {state.mobile}
               </Text>
               <View >

                       <OTPInputView
                           style={style.otpstyle}
                           pinCount={4}
                           // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                           // onCodeChanged = {code => { this.setState({code})}}
                           autoFocusOnLoad
                           codeInputFieldStyle={styles.underlineStyleBase}
                           codeInputHighlightStyle={styles.underlineStyleHighLighted}
                           onCodeFilled = {(code => {
setState({...state, code: code})

                               console.log(`Code is ${code}, you are good to go!`)
                           })}
                       />

                   </View>


                   <Text style = {style.resend}>
                   Send a new code
                   </Text>

               <TouchableOpacity style = {style.loginview}onPress={() => verifyMobile()} >
                                                                  <View>
                                                                  <Text style = {style.next}>
                                                                  SUBMIT
                                                                  </Text>

                                                                  </View>
                                                                  </TouchableOpacity>

      </ImageBackground>
    </View>
  );
};

export default Otp;
