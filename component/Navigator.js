import React, {useRef} from 'react';
import Splash from './Splash.js';
import Introduction from './Introduction.js';
import Main from './Main.js';
import Login from './Login.js';
import Otp from './Otp.js';
import Choose from './Choose.js';
import Other from './Other.js';
import DChart from './DChart.js';
import Department from './Department.js';
import Home from './Home.js';
import Help from './Help.js';
import High from './High.js';
import Forgot from './Forgot.js';
import Change from './Change.js';
import Pchart from './Pchart.js';
import Highs from './Highs.js';
import CcChart from './CcChart.js';
import Contractor from './Contractor.js';
import Chart from './Chart.js';
import Performances from './Performances.js';
import ProjectPerformance from './ProjectPerformance.js';
import Signup from './Signup.js';
import stringsoflanguages from './Language';
import {NavigationContainer} from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';

const Stack = createStackNavigator();
const Navigator = () => {
  const navigationRef = useRef();
  //stringsoflanguages.setLanguage('ur')
  // useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        // initialRouteName={'Store'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        {/* <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        /> */}
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Introduction"
          component={Introduction}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />
        <Stack.Screen
         name="Forgot"
         component={Forgot}
         options={{
         title: stringsoflanguages.forgot,
         headerStyle: {
           backgroundColor: '#8E1B56',
         },
         headerTintColor: '#fff',
         headerTitleStyle: {
           fontFamily: 'Nunito-Bold',
         },
       }}
       />
       <Stack.Screen
        name="Other"
        component={Other}
        options={{
        title: stringsoflanguages.other,
        headerStyle: {
          backgroundColor: '#8E1B56',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Nunito-Bold',
        },
      }}
      />

       <Stack.Screen
        name="Help"
        component={Help}
        options={{
        title: stringsoflanguages.help,
        headerStyle: {
          backgroundColor: '#8E1B56',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Nunito-Bold',
        },
      }}
      />
        <Stack.Screen
          name="Main"
          component={Main}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
         name="ProjectPerformance"
         component={ProjectPerformance}
         options={{
         title: stringsoflanguages.project,
         headerStyle: {
           backgroundColor: '#8E1B56',
         },
         headerTintColor: '#fff',
         headerTitleStyle: {
           fontFamily: 'Nunito-Bold',
         },
       }}
       />

       <Stack.Screen
        name="High"
        component={High}
        options={{
        title: stringsoflanguages.project,
        headerStyle: {
          backgroundColor: '#8E1B56',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Nunito-Bold',
        },
      }}
      />

      <Stack.Screen
       name="Choose"
       component={Choose}
       options={{
       title: "Select Language",
       headerStyle: {
         backgroundColor: '#8E1B56',
       },
       headerTintColor: '#fff',
       headerTitleStyle: {
         fontFamily: 'Nunito-Bold',
       },
     }}
     />

       <Stack.Screen
        name="Contractor"
        component={Contractor}
        options={{
        title: stringsoflanguages.contractor,
        headerStyle: {
          backgroundColor: '#8E1B56',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Nunito-Bold',
        },
      }}
      />
      <Stack.Screen
       name="Department"
       component={Department}
       options={{
       title: stringsoflanguages.depart,
       headerStyle: {
         backgroundColor: '#8E1B56',
       },
       headerTintColor: '#fff',
       headerTitleStyle: {
         fontFamily: 'Nunito-Bold',
       },
     }}
     />

     <Stack.Screen
      name="Change"
      component={Change}
      options={{
      title: 'Change Password',
      headerStyle: {
        backgroundColor: '#8E1B56',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontFamily: 'Nunito-Bold',
      },
    }}
    />
     <Stack.Screen
      name="Highs"
      component={Highs}
      options={{
      title: stringsoflanguages.depart,
      headerStyle: {
        backgroundColor: '#8E1B56',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontFamily: 'Nunito-Bold',
      },
    }}
    />
    <Stack.Screen
     name="CcChart"
     component={CcChart}
     options={{
     title: stringsoflanguages.contractor,
     headerStyle: {
       backgroundColor: '#8E1B56',
     },
     headerTintColor: '#fff',
     headerTitleStyle: {
       fontFamily: 'Nunito-Bold',
     },
   }}
   />
     <Stack.Screen
      name="DChart"
      component={DChart}
      options={{
      title: stringsoflanguages.depart,
      headerStyle: {
        backgroundColor: '#8E1B56',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontFamily: 'Nunito-Bold',
      },
    }}
    />
      <Stack.Screen
       name="Chart"
       component={Chart}
       options={{
       title: stringsoflanguages.contractor,
       headerStyle: {
         backgroundColor: '#8E1B56',
       },
       headerTintColor: '#fff',
       headerTitleStyle: {
         fontFamily: 'Nunito-Bold',
       },
     }}
     />

       <Stack.Screen
        name="Performances"
        component={Performances}
        options={{
        title: stringsoflanguages.performance,
        headerStyle: {
          backgroundColor: '#8E1B56',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Nunito-Bold',
        },
      }}
      />
      <Stack.Screen
       name="Pchart"
       component={Pchart}
       options={{
       title: stringsoflanguages.performance,
       headerStyle: {
         backgroundColor: '#8E1B56',
       },
       headerTintColor: '#fff',
       headerTitleStyle: {
         fontFamily: 'Nunito-Bold',
       },
     }}
     />

        {/* <Stack.Screen
          name="SelectLanguage"
          component={SelectLanguage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login2"
          component={Login2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LeaderBoard"
          component={LeaderBoard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveGames"
          component={LiveGames}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Vip"
          component={Vip}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VipDetail"
          component={VipDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Store"
          component={Store}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyLevel"
          component={MyLevel}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyEarning"
          component={MyEarning}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Support"
          component={Support}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Setting"
          component={Setting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveUser"
          component={LiveUser}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoDetails"
          component={VideoDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AudioDetails"
          component={AudioDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PartyCall"
          component={PartyCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OneCall"
          component={OneCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LiveHouse1"
          component={LiveHouse1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Viewers"
          component={Viewers}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoPlay"
          component={VideoPlay}
          options={{headerShown: false}}
        />*/}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
