import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,TextInput} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
import stringsoflanguages from './Language';
var validator = require("email-validator");
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  Forg,
} from '../backend/Api';



import * as actions from '../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const Forgot = ({navigation}) => {
  const [userid, setUserid] = useState('');
    const [password, setPassword] = useState('');


const signupButtonPress = () =>{
  navigation.navigate('Signup')
}


const onChanged  = (text) =>{
  setUserid(text)
   // this.setState({
   //     mobile: text.replace(/[^0-9]/g, ''),
   // });
}
const loginButtonPress = async () =>{

  if (userid.length == 0){
    alert('Please enter Email Address')
    return
  }
  if (validator.validate(userid) == false){
      alert('Please enter valid Email Address')
    return
  }





    Forg({Email: userid})
         .then((data) => {



           console.log(JSON.stringify(data))
           if (data.Status) {
          //   alert(data.otp)
    alert('Sucessfully Send on your Email id')

           } else {

           }
         })
         .catch((error) => {
           alert('Invalid Credentials')
           console.log('error', error);
         });

  // navigation.reset({
  //                index: 0,
  //                routes: [{name: 'Home'}],
  //              });
  // if (mobile.length == 10){
  //   LoginOtpApi({mobile: +mobile})
  //        .then((data) => {
  //          console.log(JSON.stringify(data))
  //          if (data.status) {
  //         //   alert(data.otp)
  //          navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
  //
  //          } else {
  //            navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
  //          }
  //        })
  //        .catch((error) => {
  //          console.log('error', error);
  //        });
  // }else{
  //   alert('Invalid Mobile number')
  // }



}
  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'} >


      <Image
                   style={style.introimage}
                   source={require('../resources/login-logo.png')}
               />




                                  <View style = {{marginLeft:28,width:window.width -56,marginTop:29}}>
                                  <TextInput
                                                             style={{ height: 40, width: '100%', fontSize: 16, fontFamily: 'Nunito-Bold', color: '#1D1E2C',borderBottomWidth: 1,
  borderBottomColor: "#acb1c0" }}
                                                             placeholder=  {stringsoflanguages.email}
                                                             placeholderTextColor="#1D1E2C"
                                                             onChangeText={(text) => onChanged(text)}
                                                             value={userid}

                                                         />



                         <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                <View>
                                                                <Text style = {style.next}>
                                                                {stringsoflanguages.submit}

                                                                </Text>

                                                                </View>
                                                                </TouchableOpacity>



                                                        </View>



      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Forgot;
