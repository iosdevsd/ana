import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,TextInput} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
import stringsoflanguages from './Language';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  Changs,
} from '../backend/Api';

import store from '../redux/store'
import * as actions from '../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const Change = ({navigation}) => {
  const [userid, setUserid] = useState('');
    const [password, setPassword] = useState('');
      const [passwords, setPasswords] = useState('');


const signupButtonPress = () =>{
  navigation.navigate('Signup')
}
useEffect (() =>{
//alert(JSON.stringify(store.getState().user.Email))
},[])

const onChanged  = (text) =>{
  setUserid(text.replace(/[^.a-z0-9]/gi,''))
   // this.setState({
   //     mobile: text.replace(/[^0-9]/g, ''),
   // });
}
const loginButtonPress = async () =>{

  if (password.length == 0){
    alert('Please enter password')
    return
  }

  if (passwords.length == 0){
    alert('Please enter  confirm password')
    return
  }
  if (passwords != password){
    alert('Password not Match')
    return
  }


var k = {NewPassword: password,ConfirmPassword:passwords,EmailId:store.getState().user.Email}
console.log(k)

    Changs({NewPassword: password,ConfirmPassword:passwords,EmailId:store.getState().user.Email})
         .then((data) => {
           alert(JSON.stringify(data))




             // if (Object.keys(data).length === 0 && data.constructor === Object) {
             //  alert(data)
             // }
            // return

           console.log(JSON.stringify(data))
           if (data.Status) {
  alert("Password has been changed successfully")

           } else {

           }
         })
         .catch((error) => {
           alert('Invalid Credentials')
           console.log('error', error);
         });

  // navigation.reset({
  //                index: 0,
  //                routes: [{name: 'Home'}],
  //              });
  // if (mobile.length == 10){
  //   LoginOtpApi({mobile: +mobile})
  //        .then((data) => {
  //          console.log(JSON.stringify(data))
  //          if (data.status) {
  //         //   alert(data.otp)
  //          navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
  //
  //          } else {
  //            navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
  //          }
  //        })
  //        .catch((error) => {
  //          console.log('error', error);
  //        });
  // }else{
  //   alert('Invalid Mobile number')
  // }



}
  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'} >


      <Image
                   style={style.introimage}
                   source={require('../resources/login-logo.png')}
               />


               <Text style = {{color:'black',fontFamily:'Nunito-Bold',fontSize: 36,paddingRight:30,marginTop:20,marginLeft:20}}>
                                    Change Password
                                  </Text>

                                  <View style = {{marginLeft:28,width:window.width -56,marginTop:29}}>


                                                        <TextField
                                                            label={stringsoflanguages.password}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {password}
                                                            secureTextEntry = {true}
                                                            onChangeText={(text) => setPassword(text)}


                                                        />

                                                        <TextField
                                                            label= {stringsoflanguages.confirm}
                                                            baseColor = '#acb1c0'
                                                            tintColor = '#acb1c0'
                                                            value = {passwords}
                                                            secureTextEntry = {true}
                                                            onChangeText={(text) => setPasswords(text)}


                                                        />



                         <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                <View>
                                                                <Text style = {style.next}>
                                                                {stringsoflanguages.submit}
                                                                </Text>

                                                                </View>
                                                                </TouchableOpacity>



                                                        </View>



      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Change;
