import React ,{ useEffect } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View} from 'react-native';
import style from '../style/Style.js';
import { AsyncStorageGetUser } from '../backend/Api';
import AsyncStorage from '@react-native-community/async-storage';
import * as actions from '../redux/actions';
import store from '../redux/store';
import stringsoflanguages from './Language';
import LocalizedStrings from 'react-native-localization'
const Splash = ({navigation}) => {
  const screenHandler = async () => {
     actions.Login(JSON.parse(await AsyncStorageGetUser()))
    setTimeout(() => {
      const user = store.getState().user
  if (Object.keys(user).length === 0 && user.constructor === Object) {
    navigation.replace('Choose')
  } else {
  // stringsoflanguages.setLanguage('ur')
   navigation.replace('Home')
  //navigation.replace('Highs')
  }
}, 3000);

  }
  useEffect(() => {


    var value =  AsyncStorage.getItem('code');
           value.then((e)=>{
               if (e == '' || e == null ){
            }else{
if (e == "ur"){
  stringsoflanguages.setLanguage('ur')
}else{
    stringsoflanguages.setLanguage('en')
}
            }
          })

    // var k =   AsyncStorage.getItem('code');
    // alert(JSON.stringify(k))
  //  stringsoflanguages.setLanguage('ur');
 //AsyncStorage.setItem('code', 'en');
setTimeout( () => {
      screenHandler()
 },3000);

  }, []);
  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/splash.png') }>


      </ImageBackground>
    </View>
  );
};

export default Splash;
