import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,TextInput,Dimensions,FlatList} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');

import * as actions from '../redux/actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {LoginOtpApi,SignUpApi,AsyncStorageSetUser,SignInApi,HomeApi,District,Rabi} from '../backend/Api';
import store from '../redux/store'

import Carousel from 'react-native-banner-carousel';
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;

const images = [
    "http://139.59.76.223/slider/slider-1.png",
    "http://139.59.76.223/slider/slider-2.png",
    "http://139.59.76.223/slider/slider-3.png"
];
import stringsoflanguages from './Language';
var array = [
  {
    image:require('../resources/Group3.png'),
    text:stringsoflanguages.project
  },
  {
    image:require('../resources/Group4.png'),
        text:stringsoflanguages.contractor
  },
  {
    image:require('../resources/Group5.png'),
    text:stringsoflanguages.depart
  },
  {
    image:require('../resources/Group6.png'),
      text:stringsoflanguages.other
  },
  {
    image:require('../resources/Group7.png'),
      text:stringsoflanguages.help,
  },
  {
    image:require('../resources/Group7.png'),
    text:stringsoflanguages.change,
  },

];


const Help = ({navigation}) =>{
    const [banner, setBanner] = useState([]);


    const loginButtonPress = (index) =>{
    if (index == 0){
      navigation.navigate('ProjectPerformance')
    }
  else  if (index == 1){
      navigation.navigate('Contractor')
    }
    else  if (index == 2){
        navigation.navigate('Department')
      }
      else  if (index == 5){
          navigation.navigate('Change')
        }
    else if (index == 6){
      actions.Logout({});
   navigation.reset({
                   index: 0,
                   routes: [{name: 'Login'}],
                 });
    }
    }

    const renderItem=({item,index}) => {

    return(
     <TouchableOpacity onPress={() => loginButtonPress(index)} >
      <View style = {{elevation:5,width:window.width/2- 20,height:176 ,margin:10,backgroundColor:'white',shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
        height: 0,
        width: 0
    },
    //android
    elevation: 1}}>



                    <Image
                                 style={{width:100,height:100,marginTop:10,resizeMode:'contain',alignSelf:'center'}}
                                 source={item.image}
                             />

                             <Text style = {{color:'black',alignSelf:'center',textAlign:'center',fontSize:16,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold"}}>
     {item.text}
                             </Text>



           </View>

    </TouchableOpacity>

     );
    }

useEffect (() =>{

//stringsoflanguages.setLanguage('ur')
//   HomeApi({user_id:store.getState().user.user_id,deviceID:"dd",deviceType:"android",deviceToken:"ss",model_name:"Ss",carrier_name:"Ss",device_country:"ss",device_memory:"Ss",has_notch:"ss",manufacture:"ss",ip_address:"sss"})
//        .then((data) => {
//          console.log(JSON.stringify(data))
//          if (data.status) {
//           // alert(JSON.stringify(data.banners))
//            console.log(JSON.stringify(data.banners))
// setBanner(data.banners)
//          //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//          } else {
//            alert(data.msg)
//           // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//          }
//        })
//        .catch((error) => {
//          console.log('error', error);
//        });
//
//
//
//
//
//        District({user_id:"1"})
//             .then((data) => {
//               console.log(JSON.stringify(data))
//               if (data.status) {
//               //  alert(JSON.stringify(data))
//                 console.log(JSON.stringify(data.banners))
//    actions.District(data.list);
//               //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//               } else {
//                 alert(data.msg)
//                // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//               }
//             })
//             .catch((error) => {
//               console.log('error', error);
//             });
//
//
//             Rabi({user_id:"1"})
//                  .then((data) => {
//                    console.log(JSON.stringify(data))
//                    if (data.status) {
//
//                      //console.log(JSON.stringify(data.banners))
//         actions.Rabi(data.list);
//                    //navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
//
//                    } else {
//                      alert(data.msg)
//                     // navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
//                    }
//                  })
//                  .catch((error) => {
//                    console.log('error', error);
//                  });


},[])

const logout = () =>{
  actions.Logout({});
         navigation.reset({
                         index: 0,
                         routes: [{name: 'Introduction'}],
                       });
}
const renderPage = (image, index) => {
      //    alert(JSON.stringify(image))
      return (

          <View key={index} style={{elevation:2,width:'100%'}}>
              <Image style={{ width:Dimensions.get('window').width, height: 220,resizeMode:'contain'}} source={{ uri: image }} />
          </View>


      );
  }

  return (
    <SafeAreaView style = {style.container}>
    <KeyboardAwareScrollView style = {{color:'white'}}>
    <Text style = {{color:'black',fontSize:16,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Ashghal Quality Tracking System - AQTS

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Ashghal Quality Tracking System – AQTS is a tool developed by QSD-ANAS S.p.A for the evaluation of the performances of Ashghal Road Projects and the Key Supply Chain Partners in the State of Qatar. Four Categories are assessed as follows:

    </Text>

    <Text style = {{color:'black',fontSize:16,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Speedometers and KPIs

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Six Families of Quality Indicators (Speedometers) have been defined for the evaluation:
Three related to material Testing results (Source: LDTS Database):

    </Text>

    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Unbound Test Results Conformity: all the test results related to unbound material as per the QCS/Specification limits;

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Asphalt Test Results Conformity: all the test results related to asphalt material as per the

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    As Constructed Conformity: all the test results related to “as built” material as per the QCS/Specification limits.

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Three related to on site and documental audits (Source: QSD-ANAS Audits Database):

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
Quality Management Score: Percentage of compliance of documental Audits related to the Quality Management System;

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
Factory Production Control Score: Percentage of compliance of on-site Audits related to the Production facilities (Asphalt plants, Wet Mixing Plants, Crushers,…);

    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Construction Practices Score: Percentage of compliance of on-site Audits related to the in-place activities during laying the material.
    The results shown in the six speedometers are related to the results from the beginning of the work to the latest update of the relevant database. Tapping the preferred speedometer trend analysis of the results are calculated.


    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Trend Analysis
    For each family of Quality Indicator (Speedometer) has been defined with trend analysis evaluation from the starting day of data collection for each project, department or KSCP.
    Results for each category are displayed as average of the relevant month.



    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Color Code
    Six different colors have been defined for the interpretation of the speedometers:
    Grey: the KPI result for that category is performing below 95%.
    5 Shades of Green: the KPI result for that category is performing from 95% to 100%. More the green is dark, more the results is close to 100%.




    </Text>
    <Text style = {{color:'#4A4956',fontSize:13,marginTop:10,marginBottom:10,fontFamily:"Nunito-Bold",marginLeft:12}}>
    Other Services
    The relevant section contains the other services administrated by Quality and Safety Department:
    QA/QC Site Questionnaire: link to the software.
    Qatar Future Roads: link to “Qatar Future Roads” website.
    Recycled Materials: link to Ashghal Services portal Material Management system.
    Approved Test List: link to Ashghal Services portal list of approved tests.




    </Text>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )

}
export default Help;
