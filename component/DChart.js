import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,Dimensions,FlatList} from 'react-native';
import style from '../style/Style.js';
const window = Dimensions.get('window');
const BASE_URL = 'http://www.files.aqts.com.qa/api/';
import MapView, { Marker } from 'react-native-maps';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import stringsoflanguages from './Language';
import moment from  'moment';
import ProgressCircle from 'react-native-progress-circle'
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'react-native-material-textfield-plus';
import GetLocation from 'react-native-get-location';

import {
  PATH_URL,
} from '../backend/Config';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
  SignUps,
  Perofrmance,
  Perofrmance2,
  Charts,
  DCharts
} from '../backend/Api';

const Dchart = ({navigation,route}) => {
  const [name, setName] = useState('');
    const [email, setEmail] = useState('');
      const [company, setCompany] = useState('');
      const [latitude, setLatiude] = useState(0);
      const [longitude, setLongitude] = useState(0);
        const [description, setDescription] = useState('');
        const [marker,setMarker] = useState([
        ])

useEffect (() =>{
//  alert(JSON.stringify(route.params.item))

  var project = JSON.stringify(PATH_URL.Project)
  console.log("22"+project)

var c = `${BASE_URL}${PATH_URL.Dc}=${route.params.item.Id}`;


DCharts(c)
     .then((data) => {
       if (data == null){
         setMarker([])
       }else{
        setMarker(data)
       }
//  alert(JSON.stringify(data))
       if (data.Status) {

         //setMarker
      //   alert(data.otp)


       } else {

       }
     })
     .catch((error) => {
       console.log('error', error);
     });
// http://www.files.aqts.com.qa/api/"ProjectPerformance/GetProjectPerformance?UserId"=1

//alert(JSON.stringify(PATH_URL))
//   GetLocation.getCurrentPosition({
//       enableHighAccuracy: true,
//       timeout: 15000,
//   })
//   .then(location => {
//       console.log(location);
// setLatiude(location.latitude)
// setLongitude(location.longitude)
//       // const [latitude, setLatiude] = useState(0);
//       // const [longitude, setLongitude] = useState(0);
//   })
//   .catch(error => {
//       const { code, message } = error;
//       console.warn(code, message);
//   })
},[])

const high = (item) =>{

 navigation.navigate('Highs',{
   item:item
 })
}


const renderItemProducts = ({ item, index }) => {


  var k = parseInt(item.PercentageYes)
var color = ""

  if (k >= 99 && k <= 100){
    color = "#009900"
  }
  if (k >= 98 && k <= 99){
    color = "#00ff01"
  }
  if (k >= 97 && k <= 98){
    color = "#66ff66"
  }
  if (k >= 96 && k <= 97){
    color = "#80ff80"
  }
  if (k >= 96 && k <= 95){
    color = "#99ff99"
  }
  if (k >= 0 && k <= 95){
    color = "#b3ffb3"
  }
    // alert(JSON.stringify(item))
    return (

      <TouchableOpacity onPress={() => high(item)} >
            <View style = {{backgroundColor:'white',width:Dimensions.get('window').width/2 - 10,margin:5,height:180,borderWidth:0}}>



      <View style = {{margin:8,alignself:'center',marginLeft:'15%'}}>
      <ProgressCircle
                  percent={item.PercentageYes}
                  radius={50}
                  borderWidth={8}
                  color= {color}
                  shadowColor="#999"
                  bgColor="#fff"
              >
                  <Text style={{ fontSize: 16 ,color:'green',fontWeight:'bold'}}>{item.PercentageYes.toFixed(2)} %</Text>
              </ProgressCircle>
              </View>

              <Text style = {{color:'black',fontSize:13,fontFamily:'Nunito-Bold',marginTop:7,textalign:'center',alignSelf:'center'}}>
              {item.CategoryName}

              </Text>



      </View>


      </TouchableOpacity>
    )
}
  return (

<View style = {{backgroundColor:'white'}}>

<View style = {{elevation:5,width:'95%',alignself:'center',marginBottom:20,backgroundColor:'white',margin:10}}>
<Text style = {{color:'#4A4956',fontSize:16,fontFamily:'Nunito-Bold',marginTop:7,margin:8,textalign:'center'}}>
{stringsoflanguages.project}: {route.params.item.DepartmentName}

</Text>
<Text style = {{color:'#4A4956',fontSize:16,fontFamily:'Nunito-Bold',margin:8,marginTop:1,textalign:'center'}}>
{stringsoflanguages.date}: {moment().format("DD/MM/YYYY")}

</Text>

</View>

<FlatList style = {{marginTop:20}}
                        data={marker}
                        numColumns={2}



                        renderItem={renderItemProducts}
                    />

                    {marker.length == 0 && (
                      <Text style = {{color:'#4A4956',fontSize:16,fontFamily:'Nunito-Bold',margin:8,marginTop:100,alignSelf:'center'}}>
                    No Data Found

                      </Text>
                    )}
</View>

  );
};

export default Dchart;
