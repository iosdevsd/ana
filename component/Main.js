import React ,{ useEffect } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity} from 'react-native';
import style from '../style/Style.js';

const Main = ({navigation}) => {


  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/background_slide.png') }>
      <Image
                   style={style.mainlogo}
                   source={require('../resources/login-logo.png')}
               />
               <Text style = {style.welcome}>
              Welcome to Wiseprep
               </Text>
               <View style = {style.mainbottomview} >
                                                                  <View style = {style.introdu}>
                                                                  <Image
                                                                               style={style.mobiles}
                                                                               source={require('../resources/mobile.png')}
                                                                           />
                                                                  <Text style = {style.mobiletexts}>
                                                                  Continue with Mobile No.
                                                                  </Text>


                                                                  </View>
                                                                  <Text style = {style.continue}>
                                                              ----  Or Continue with  ----
                                                                  </Text>

                                                                  <View style = {style.introdu}>

                                                                  <View style = {style.facebookview}>
                                                                  <Image
                                                                               style={style.mobile}
                                                                               source={require('../resources/fb.png')}
                                                                           />
                                                                  <Text style = {style.mobiletext}>
                                                                  Facebook
                                                                  </Text>
                                                                    </View>
                                                                    <View style = {style.googleview}>
                                                                    <Image
                                                                                 style={style.mobile}
                                                                                 source={require('../resources/google.png')}
                                                                             />
                                                                    <Text style = {style.googletext}>
                                                                    Google
                                                                    </Text>
                                                                      </View>
                                                                  </View>


<View style = {style.introdu}>
<Text style = {style.byclick}>
By Clicking SignUp , you agree to
<Text style = {style.term}>
&nbsp;Our Terms and Privacy Policy.
</Text>
</Text>


</View>


                                                                    </View>

      </ImageBackground>
    </View>
  );
};

export default Main;
