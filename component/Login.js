import React ,{ useEffect,useState } from 'react';
import {SafeAreaView, Text,ImageBackground,Image,View,TouchableOpacity,TextInput} from 'react-native';
import style from '../style/Style.js';
import {LoginOtpApi} from '../backend/Api';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const Login = ({navigation}) => {
const [mobile, setMobile] = useState('');


const loginButtonPress = async () =>{
  navigation.reset({
                 index: 0,
                 routes: [{name: 'Home'}],
               });




  // if (mobile.length == 10){
  //   LoginOtpApi({mobile: +mobile})
  //        .then((data) => {
  //          console.log(JSON.stringify(data))
  //          if (data.status) {
  //         //   alert(data.otp)
  //          navigation.navigate('Otp',{otp:+data.otp,status:true,mobile:+mobile})
  //
  //          } else {
  //            navigation.navigate('Otp',{otp:+data.otp,status:false,mobile:+mobile})
  //          }
  //        })
  //        .catch((error) => {
  //          console.log('error', error);
  //        });
  // }else{
  //   alert('Invalid Mobile number')
  // }



}

  return (
    <View style = {style.container}>
      <ImageBackground style = {style.imagebackground}
      source = {require('../resources/background_slide.png') }>
      <Image
                   style={style.loginlogo}
                   source={require('../resources/login-logo.png')}
               />
               <Text style = {style.loginwith}>
              Login with mobile number
               </Text>
               <Text style = {style.introtexts}>
              Enter your mobile number we will sent you OTP to verify
               </Text>

               <Text style = {style.introtextss}>
              Mobile Number
               </Text>
               <TextInput placeholder="Enter Mobile Number" style={styles.textInput}
               onChangeText={(text) => setMobile(text)}
              value={mobile}
               ></TextInput>

               <TouchableOpacity style = {style.loginview}onPress={() => loginButtonPress()} >
                                                                  <View>
                                                                  <Text style = {style.next}>
                                                                  LOGIN
                                                                  </Text>

                                                                  </View>
                                                                  </TouchableOpacity>

      </ImageBackground>
    </View>
  );
};

export default Login;
